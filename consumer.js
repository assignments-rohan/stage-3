const { execFile } = require('child_process');
const amqp = require('amqplib/callback_api');
let amqpConn = null;

amqp.connect('amqp://localhost', function (err, conn) {
    if (err) {
        return console.error("[AMQP]", err.message);
    }
    conn.on("error", function (err) {
        if (err.message !== "Connection closing") {
            console.error("[AMQP] conn error", err.message);
        }
    });
    conn.on("close", function () {
        process.kill(process.pid);
    });
    console.log("[AMQP] connected");
    amqpConn = conn;
    console.log('connected...');
    consumeUrl();
});

async function consumeUrl() {
    const channel = await amqpConn.createChannel();
    await channel.assertQueue('jobs');

    channel.consume("jobs", message => {
        const obj = JSON.parse(message.content.toString());
        service(obj.url);
    });

}

async function service(url) {
    execFile('node', ['request.js', url], (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
        amqpConn.close();
    });
}